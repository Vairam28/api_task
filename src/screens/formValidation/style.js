import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    root:{
        backgroundColor:"#F5F7FA ",
        padding:"16px 24px",
        height:"100vh",
        overflow:"auto",
        position:"relative"
    },
    labelText:{
        color:"#98A0AC",
        fontSize:"12px",
        paddingBottom:"8px",
        fontFamily:"sans-serif",
    },
    labelTitle:{
        color:"#4E5A6B",
        fontSize:"16px",
        paddingBottom:"8px",
        fontFamily:"sans-serif",
        fontWeight:"600",
    },
    bottomSection:{
        backgroundColor:"white",
        padding:"12px",
        position:"fixed",
        display:"flex",
        justifyContent:"end",
        width:"100%",
        bottom:0,
        left:0,
        right:0
    },
    fieldBackGrnd:{
        backgroundColor:"white",
        padding:"12px",
        borderRadius:"8px",
        marginTop:"8px",
        boxShadow:"0px 10px 25px #0000000A",
    },
    cancelBtn:{
        marginRight:"10px"
    },
    redStar:{
        color:"red",
        fontSize:"12px"
    },
    tableSection:{
        marginTop:"20px",
    }


}))