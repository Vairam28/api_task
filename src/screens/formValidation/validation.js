import React, { useState, useEffect } from 'react';
import { Box, Button, Grid, Typography, Table, TableBody, TableCell, TableContainer, TableHead, TableRow, Paper } from '@mui/material';
import { FormInput } from '../../components';
import { useStyles } from './style';
import { Edit } from '@mui/icons-material';
import DeleteIcon from '@mui/icons-material/Delete';
import axios from 'axios';

const initial = {
    name: '',
    age: '',
    dateOfBirth: '',
    mail: '',
    number: '',
    error: {
        mail: '',
        age: '',
        number: '',
        dateOfBirth: '',
        name: '',

    }
}

export const Validation = props => {

    const classes = useStyles();
    const [data, SetData] = useState({ ...initial });
    const [dataArr] = useState([]);
    const [index, SetIndex] = useState(null);
    const [getAllData, SetgetAllData] = useState([]);
    
    React.useEffect(() => {

    })

    // updating state
    const updateState = (key, value) => {
        let error = data.error
        error[key] = ""
        SetData({ ...data, [key]: value, error });
    }

    // on submit 
    const submit = () => {
        debugger
        if (validate()) {

            const form = {

                name: data?.name,
                age: data?.age,
                dateOfBirth: data?.dateOfBirth,
                mail: data?.mail,
                number: data?.number,
            };
            if(index>=0 && index!== null){
                const editData = getAllData[index]
                axios.put(`https://63318f15cff0e7bf70f0af61.mockapi.io/api/V1/users/${editData?.id}`, 
                form
            ).then(function (response) {
                console.log(response?.data)
                getData()
                SetIndex(null)
                SetData({
                    ...initial
                })
            });

            }
            else{    
                axios.post('https://63318f15cff0e7bf70f0af61.mockapi.io/api/V1/users', 
                form
            ).then(function (response) {
                debugger
                console.log(response?.data)
                getData()
            })

            }


            console.log(dataArr, "setData");
            SetData({
                name: "",
                age: "",
                dateOfBirth: '',
                mail: "",
                number: "",
                error: {
                    name: "",
                    age: "",
                    dateOfBirth: '',
                    mail: "",
                    number: ""
                }
            })
        }

    }
    //  on edit 
    const editIcon = (row, index) => {
        SetIndex(index);
        SetData({
            ...data,
            ...row,
        })

    }

    // on delete
    const deleteTableRows = (row, index) => {
        axios.delete(`https://63318f15cff0e7bf70f0af61.mockapi.io/api/V1/users/${row?.id}`)
        .then(function (response) {
            getData();
        })
    }

    // validation form cancelling
    const clear = () => {
        SetData({ ...initial });
    }

    // validation form
    const validate = () => {
        let isValid = true;
        let error = data.error

        if (data?.name.length === 0) {
            isValid = false
            error.name = "Name is required"
        }
        if (data?.age.length === 0) {
            isValid = false
            error.age = "Age is required"
        }
        if (data?.mail.length === 0) {
            isValid = false
            error.mail = "Mail- ID is required"
            
        }
        if (data?.dateOfBirth.length === 0) {
            isValid = false
            error.dateOfBirth = "Date of Birth is required"
            
        }
        if (data?.number.length === 0) {
            isValid = false
            error.number = "Mobile Number is required"
            
        }
        if (data?.mail.length > 0 && !/^\w+([-]?\w+)*@\w+([-]?\w+)*(\.\w{2,3})+$/.test(data?.mail)) {
            isValid = false
            error.mail = "Please Fill Valid Mail ID"
           
        }
        if (data?.age.length > 0 && !/^[0-9\b]+$/.test(data?.age)) {
            isValid = false
            error.age = "Please Fill Valid Age"
            
        }
        if (data?.number.length > 0 && !/^(\+\d{1,3}[- ]?)?\d{10}$/.test(data?.number)) {
            isValid = false
            error.number = "Please Fill Valid Number"
            
        }
        SetData({ ...data, error })
        return isValid
    }

    const getData = () => {
  
        axios.get('https://63318f15cff0e7bf70f0af61.mockapi.io/api/V1/users')
        .then(function (response) {
            console.log(response, "getData")
            SetgetAllData(response?.data)
        })

    }
    // useEffect 
    useEffect(() => {
      getData()
    
      
    }, [])
    
    return (
        <>
            <div className={classes.root}>
                <Typography className={classes.labelTitle}>Personal Details</Typography>
                {/* text fields */}
                <Grid container spacing={1.5} className={classes.fieldBackGrnd}>
                    <Grid item lg={4} md={4} sm={4} xs={12} >
                        <Typography className={classes.labelText}>Name <span className={classes.redStar}>*</span></Typography>
                        <FormInput value={data?.name} onChange={(e) => updateState("name", e.target.value)}
                            error={data.error.name.length > 0}
                            errorMsg={data.error.name}
                        />
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={12} >
                        <Typography className={classes.labelText}>Age <span className={classes.redStar}>*</span></Typography>
                        <FormInput value={data?.age}
                            onChange={(e) => updateState("age", e.target.value)}
                            error={data.error.age.length > 0}
                            errorMsg={data.error.age}
                        />
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={12} >
                        <Typography className={classes.labelText}>Date of Birth <span className={classes.redStar}>*</span></Typography>
                        <FormInput value={data?.dateOfBirth} onChange={(e) => updateState("dateOfBirth", e.target.value)}
                            error={data.error.dateOfBirth.length > 0}
                            errorMsg={data.error.dateOfBirth}
                        />
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={12} >
                        <Typography className={classes.labelText}>Mail - ID <span className={classes.redStar}>*</span></Typography>
                        <FormInput value={data?.mail} onChange={(e) => updateState("mail", e.target.value)}
                            error={data.error.mail.length > 0}
                            errorMsg={data.error.mail}
                        />
                    </Grid>

                    <Grid item lg={4} md={4} sm={4} xs={12} >
                        <Typography className={classes.labelText}>Mobile Number <span className={classes.redStar}>*</span></Typography>
                        <FormInput value={data?.number} onChange={(e) => updateState("number", e.target.value)}
                            error={data.error.number.length > 0}
                            errorMsg={data.error.number}
                        />
                    </Grid>

                </Grid>
                {/* text fields  */}
                {/* table */}
                <Grid container spacing={1} className={classes.tableSection}>
                    <TableContainer component={Paper}>
                        <Table sx={{ minWidth: 650 }} aria-label="simple table">
                            <TableHead>
                                <TableRow>
                                    <TableCell >Name</TableCell>
                                    <TableCell >Age</TableCell>
                                    <TableCell >Mail -ID</TableCell>
                                    <TableCell >Mobile Number</TableCell>
                                    <TableCell >Date of Birth</TableCell>
                                    <TableCell >Edit</TableCell>
                                    <TableCell >Delete</TableCell>
                                </TableRow>
                            </TableHead>
                            <TableBody>
                                {getAllData.map((row, index) => (
                                    <TableRow
                                        key={row.id}
                                        sx={{ '&:last-child td, &:last-child th': { border: 0 } }}
                                    >
                                        <TableCell component="th" scope="row">
                                            {row.name}
                                        </TableCell>
                                        <TableCell >{row.age}</TableCell>
                                        <TableCell >{row.mail}</TableCell>
                                        <TableCell >{row.number}</TableCell>
                                        <TableCell >{row.dateOfBirth}</TableCell>
                                        <TableCell >
                                            <Edit onClick={() => editIcon(row, index)} />
                                        </TableCell>
                                        <TableCell >
                                            <DeleteIcon onClick={() => deleteTableRows(row, index)} />
                                        </TableCell>
                                    </TableRow>
                                ))}
                            </TableBody>
                        </Table>
                    </TableContainer>
                </Grid>
                {/* table */}
                {/* footer */}
                <Box className={classes.bottomSection}>
                    <Button onClick={()=>clear()} className={classes.cancelBtn} variant='outlined' >Cancel</Button>
                    <Button onClick={()=>submit()} className={classes.createBtn} variant='contained'>{index>=0 && index!==null ? "Update":"Create"}</Button>
                </Box>
                {/* footer */}
            </div>
        </>
    )
}