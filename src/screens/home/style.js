import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    rightSideDash: {
        backgroundColor: "#F3F5F9",
        padding: "20px",
    },
    leftSideDash: {
        padding: "20px",
        paddingBottom: "44px"
    },
    quotes:{
        fontWeight: "bold",
        fontFamily:"crayond_bold"
    },
    profileName:{
        color:"#1A2131",
        fontSize:"18px",
        fontFamily:"crayond_bold"
    },
    profitTitle:{
        color:"#1A2131",
        fontSize:"14px",
    },
    searchIcon: {
        backgroundColor: "#FFFFFF",
        color: "black",
        borderRadius: "50%",
        display: "flex",
        height: "32px",
        width: "32px",
        alignItems: "center",
        justifyContent: "center",
        
        "& .css-i4bv87-MuiSvgIcon-root":{
            width: "22px",
        }
    },
    settingIcon:{
        backgroundColor: "#F3F5F9",
        color: "black",
        borderRadius: "50%",
        display: "flex",
        height: "32px",
        width: "32px",
        alignItems: "center",
        justifyContent: "center",
        
        "& .css-i4bv87-MuiSvgIcon-root":{
            width: "22px",
        }
    },
    seeAll:{
        color:"#5E9BFF",
        fontWeight:"bold",
        cursor:"pointer"
    },
    favour:{
        color:"black",
        fontFamily:"crayond_bold"
    },
    paddingAdjust:{
        paddingLeft:"58px !important",
        [theme.breakpoints.only("sm")]: {
            paddingLeft:"50px !important"
         },
        [theme.breakpoints.only("xs")]: {
           paddingLeft:"16px !important"
        }
    },
    multipleTabs:{
        borderTop:"1px solid lightgrey",
        borderBottom:"1px solid lightgrey",
        padding:"20px"
    },
    activeState:{
        color:"#050505",
        fontWeight:"bold"
    },
    unactiveState:{
        color:"lightgrey"
    },
    activityGraph:{
        color:"#79828B",
        fontSize:"14px",
    },
    activityDollar:{
        fontWeight:"bold",
        color:"#050505",
        fontSize:"20px",
    },
    graphParent:{
        padding:"20px",
        borderRight:"1px solid lightgrey"
    },
    activityGraphImg:{
        width:"100%"
    },
    parentTabHeader:{
        padding:"20px",
        paddingTop:"11px"
    },
    addLogo: {
        backgroundColor: "#2C00D5",
        color: "white",
        borderRadius: "8px",
        display: "flex",
        height: "32px",
        width: "32px",
        alignItems: "center",
        justifyContent: "center"
    },
    tabPadding:{
        paddingTop:"16px",
        "& .css-19kzrtu":{
            padding:"0px"
        }
    },
    dateAndYear:{
        fontSize:"12px",
        paddingBottom:"8px",
        color:"#C1C2C6"
    },
}))