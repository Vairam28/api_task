import React from 'react';
import { Typography, Grid, Box, Stack, Avatar } from '@mui/material';
import { useStyles } from "./style";
import BackupTableIcon from '@mui/icons-material/BackupTable';
import SearchIcon from '@mui/icons-material/Search';
import NotificationsNoneOutlinedIcon from '@mui/icons-material/NotificationsNoneOutlined';
import { DollarCard, VisaCard, SettingsCard, HistoryCard, LivePrices, Portfolio, Bitcoin, DropIcon } from "../../components"
import CurrencyBitcoinIcon from '@mui/icons-material/CurrencyBitcoin';
import ChargingStationOutlinedIcon from '@mui/icons-material/ChargingStationOutlined';
import PrivacyTipOutlinedIcon from '@mui/icons-material/PrivacyTipOutlined';
import EmergencyShareOutlinedIcon from '@mui/icons-material/EmergencyShareOutlined';
import TimelineIcon from '@mui/icons-material/Timeline';
import WidgetsOutlinedIcon from '@mui/icons-material/WidgetsOutlined';
import AutoStoriesOutlinedIcon from '@mui/icons-material/AutoStoriesOutlined';
import ShoppingCartCheckoutOutlinedIcon from '@mui/icons-material/ShoppingCartCheckoutOutlined';
import ChevronRightIcon from '@mui/icons-material/ChevronRight';
import CardGiftcardIcon from '@mui/icons-material/CardGiftcard';
import SettingsOutlinedIcon from '@mui/icons-material/SettingsOutlined';
import AddTaskOutlinedIcon from '@mui/icons-material/AddTaskOutlined';
import ShoppingBagOutlinedIcon from '@mui/icons-material/ShoppingBagOutlined';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import PropTypes from 'prop-types';
import AddOutlinedIcon from '@mui/icons-material/AddOutlined';
import CompareArrowsIcon from '@mui/icons-material/CompareArrows';
import AccountBalanceOutlinedIcon from '@mui/icons-material/AccountBalanceOutlined';
import MonetizationOnOutlinedIcon from '@mui/icons-material/MonetizationOnOutlined';
import EuroOutlinedIcon from '@mui/icons-material/EuroOutlined';
import PixOutlinedIcon from '@mui/icons-material/PixOutlined';

function TabPanel(props) {
    const { children, value, index, ...other } = props;

    return (
        <div
            role="tabpanel"
            hidden={value !== index}
            id={`simple-tabpanel-${index}`}
            aria-labelledby={`simple-tab-${index}`}
            {...other}
        >
            {value === index && (
                <Box sx={{ p: 3 }}>
                    <Typography>{children}</Typography>
                </Box>
            )}
        </div>
    );
}

TabPanel.propTypes = {
    children: PropTypes.node,
    index: PropTypes.number.isRequired,
    value: PropTypes.number.isRequired,
};

function a11yProps(index) {
    return {
        id: `simple-tab-${index}`,
        'aria-controls': `simple-tabpanel-${index}`,
    };
}

export const Home = props => {

    const classes = useStyles();

    const bitcoinData = [
        {
            title: "BTC",
            subTitle: "Bitcoin",
            dollar: "$20,010",
            percentage: "+0.25%",
            img: "/images/blueGraph.png",
            coinBackground: "black",
            icon: <CurrencyBitcoinIcon />,
        },
        {
            title: "ETH",
            subTitle: "Ethereum",
            dollar: "$21,581",
            percentage: "+1.57%",
            img: "/images/yellowGraph.png",
            coinBackground: "#FF9E44",
            icon: <ChargingStationOutlinedIcon />,
        },
    ]

    const livePrices = [
        {
            title: "Binanace",
            subTitle: "BNB",
            amount: "$10,800",
            percentage: "+0.15%",
            img: "/images/greenChart.png",
            backgroundColor: "#9ED0A6",
            icon: <PrivacyTipOutlinedIcon />,
            fontColor: "#80B99E"
        },
        {
            title: "Litecoin",
            subTitle: "LTC",
            amount: "$76,560",
            percentage: "+2.57%",
            img: "/images/blueChart.png",
            backgroundColor: "#2C01D7",
            icon: <EmergencyShareOutlinedIcon />,
            fontColor: "#80B99E"
        },
        {
            title: "Ethereum",
            subTitle: "ETH",
            amount: "$86,022",
            percentage: "-1.37%",
            img: "/images/yellowChart.png",
            backgroundColor: "#FF9E44",
            icon: <ChargingStationOutlinedIcon />,
            fontColor: "red"
        },
        {
            title: "Bitcoin",
            subTitle: "BTC",
            amount: "$26,560",
            percentage: "+3.17%",
            img: "/images/blackChart.png",
            backgroundColor: "black",
            icon: <CurrencyBitcoinIcon />,
            fontColor: "#80B99E"
        }
    ]

    const dollarCard = [
        {
            title: "Total earnings",
            subTitle: "$12,587.10",
            backgroundColor: "#9CCEA6",
            icon: <TimelineIcon />
        },
        {
            title: "Goal for this month",
            subTitle: "$16,196.80",
            backgroundColor: "#F0F3DE",
            icon: <WidgetsOutlinedIcon />
        }
    ]

    const dollarCardRight = [
        {
            title: "Total Spendings",
            subTitle: "$43,123.85",
            backgroundColor: "#FFC895",
            icon: <ShoppingCartCheckoutOutlinedIcon />
        },
        {
            title: "Spending Goal",
            subTitle: "$32,281.18",
            backgroundColor: "#BBECFE",
            icon: <AutoStoriesOutlinedIcon />
        }
    ]

    const settings = [
        {
            title: "Monthly Plan",
            arrow: <ChevronRightIcon />,
            icon: <CardGiftcardIcon />
        },
        {
            title: "Settings",
            arrow: <ChevronRightIcon />,
            icon: <SettingsOutlinedIcon />
        },
        {
            title: "Goals",
            arrow: <ChevronRightIcon />,
            icon: <AddTaskOutlinedIcon />
        },
        {
            title: "Shopping",
            arrow: <ChevronRightIcon />,
            icon: <ShoppingBagOutlinedIcon />
        },

    ]

    const historyCard = [
        {
            title: "Office Supplies",
            subTitle: "10 sep 2021 at 2:20 PM",
            jobTitle: "Supplies",
            dollarNumber: "-$10,865",
            icon: <CompareArrowsIcon />,
            dot: "red",
            dotBackground: "#FFF6F5",
            amountColor: "#2D2D2D",
            iconColor: "dimgrey"
        }
    ]

    const historyCardSecond = [
        {
            title: "Pak Transfer",
            subTitle: "28 nov 2021 at 8:18 AM",
            jobTitle: "Marketing",
            dollarNumber: "-$10,865",
            icon: <AccountBalanceOutlinedIcon />,
            dot: "#3F25C3",
            dotBackground: "#F0FCF8",
            amountColor: "blueviolet",
            iconColor: "blueviolet"
        },
        {
            title: "Salary Transfer",
            subTitle: "28 nov 2021 at 12:55 PM",
            jobTitle: "Office Supplies",
            dollarNumber: "-$52,810",
            icon: <MonetizationOnOutlinedIcon />,
            dot: "#BB49E3",
            dotBackground: "#FBF4FD",
            amountColor: "#2D2D2D",
            iconColor: "dimgrey"
        },
        {
            title: "Cash Withdrawl",
            subTitle: "28 nov 2021 at 06:20 PM",
            jobTitle: "General banking",
            dollarNumber: "-$27,309",
            icon: <EuroOutlinedIcon />,
            dot: "#12A8B1",
            dotBackground: "#E7FFFD",
            amountColor: "#2D2D2D",
            iconColor: "dimgrey"
        },

    ]

    const historyCardThird = [
        {
            title: "Target",
            subTitle: "18 dec 2021 at 6:33 PM",
            jobTitle: "Equipment",
            dollarNumber: "-$64,149",
            icon: <PixOutlinedIcon />,
            dot: "#EB9B50",
            dotBackground: "#FFF5D9",
            amountColor: "#2D2D2D",
            iconColor: "dimgrey"
        }
    ]
    const [value, setValue] = React.useState(0);

    const handleChange = (event, newValue) => {
        setValue(newValue);
    };

    return <div className={classes.root}>
        <Grid container>
            {/* left side card */}
            <Grid item xs={12} sm={12} md={12} lg={8}>
                <Box className={classes.leftSideDash}>
                    <Stack direction="row" spacing={2} display={"flex"} justifyContent={"space-between"} alignItems={"center"}>
                        <Stack direction="row" alignItems={"center"} spacing={2}>
                            <Avatar alt="Remy Sharp" src="/images/sachin.webp" />
                            <Typography className={classes.quotes}>Good Evening Vairam!</Typography>
                        </Stack>
                        <Stack direction="row" alignItems={"center"} spacing={2}>
                            <DropIcon />
                            <Box className={classes.addLogo}>
                                <BackupTableIcon />
                            </Box>
                        </Stack>
                    </Stack>
                    {/* visacard and small cards */}
                    <Grid container spacing={2} >
                        <Grid item xs={12} sm={12} md={12} lg={12}>
                            <Grid container spacing={2} >
                                <Grid item xs={12} sm={3.5} md={3.5} lg={3.5}>
                                    {dollarCard?.map((val) => {
                                        return (
                                            <DollarCard val={val} />
                                        )
                                    })
                                    }
                                </Grid>
                                <Grid item xs={12} sm={5} md={5} lg={5}>
                                    <VisaCard />
                                </Grid>
                                <Grid item xs={12} sm={3.5} md={3.5} lg={3.5} className={classes.paddingAdjust} >
                                    {dollarCardRight?.map((val) => {
                                        return (
                                            <DollarCard val={val} />
                                        )
                                    })
                                    }
                                </Grid>
                            </Grid>
                        </Grid>
                    </Grid>
                    {/* visacard and small cards */}
                </Box>
                <Box className={classes.multipleTabs}>
                    <Stack direction="row" display={"flex"} alignItems={"center"} spacing={6} >
                        <Typography className={classes.activeState}>
                            ACTIVITY SUMMARY
                        </Typography>
                        <Typography className={classes.unactiveState}>
                            SPENDING SUMMARY
                        </Typography>
                        <Typography className={classes.unactiveState}>
                            INCOME SUMMARY
                        </Typography>
                    </Stack>
                </Box>
                {/* activity graph and history */}
                <Grid container>
                    <Grid item xs={12} sm={12} md={4.5} lg={4.5}>
                        <Stack className={classes.graphParent}>
                            <Box display={"flex"} justifyContent={"space-between"} alignItems={"center"}>
                                <Typography className={classes.activityGraph}>Activity Graph</Typography>
                                <Typography className={classes.activityDollar}>$186k</Typography>
                            </Box>
                            <Box pb={2}>
                                <img src="/images/activityGraph.png" alt="grp" />
                            </Box>
                            {
                                settings?.map((val) => {
                                    return (
                                        <SettingsCard val={val} />
                                    )
                                })
                            }
                        </Stack>
                    </Grid>
                    <Grid item xs={12} sm={12} md={7.5} lg={7.5}>
                        <Stack className={classes.parentTabHeader}>
                            <Box display={"flex"} justifyContent={"space-between"} alignItems={"center"}>
                                <Box sx={{ borderBottom: 1, borderColor: 'divider' }}>
                                    <Tabs value={value} onChange={handleChange} aria-label="basic tabs example" className={classes.tabParent}>
                                        <Tab className={classes.switchingTab} label="History" {...a11yProps(0)} />
                                        <Tab className={classes.switchingTab} label="Upcoming" {...a11yProps(1)} />
                                    </Tabs>
                                </Box>
                                <Stack direction="row" spacing={2} >
                                    <Box className={classes.settingIcon}>
                                        <SettingsOutlinedIcon />
                                    </Box>
                                    <Box className={classes.addLogo}>
                                        <AddOutlinedIcon />
                                    </Box>
                                </Stack>
                            </Box>
                            {/* history */}
                            <TabPanel value={value} index={0} className={classes.tabPadding}>
                                <Stack>
                                    <Typography className={classes.dateAndYear}>21 May 2021</Typography>
                                    {
                                        historyCard?.map((val) => {
                                            return (
                                                <HistoryCard val={val} />
                                            )
                                        })
                                    }
                                </Stack>
                                <Stack>
                                    <Typography className={classes.dateAndYear}>28 Nov 2021</Typography>
                                    {
                                        historyCardSecond?.map((val) => {
                                            return (
                                                <HistoryCard val={val} />
                                            )
                                        })
                                    }
                                </Stack>
                                <Stack>
                                    <Typography className={classes.dateAndYear}>18 Dec 2021</Typography>
                                    {
                                        historyCardThird?.map((val) => {
                                            return (
                                                <HistoryCard val={val} />
                                            )
                                        })
                                    }
                                </Stack>
                            </TabPanel>
                            {/* history */}
                            {/* upcoming */}
                            <TabPanel value={value} index={1}>

                            </TabPanel>
                            {/* upcoming */}
                        </Stack>
                    </Grid>
                </Grid>
                {/* activity graph and history */}
            </Grid >
            {/* left side card */}
            {/* right side card */}
            <Grid item xs={12} sm={12} md={12} lg={4} >
                <Box className={classes.rightSideDash}>
                    <Stack direction="row" display={"flex"} justifyContent={"space-between"} alignItems={"center"} pb={5} >
                        <Box>
                            <Typography className={classes.profileName}>
                                Orizon Crypto
                            </Typography>
                            <Typography className={classes.profitTitle}>
                                Increase your profit
                            </Typography>
                        </Box>
                        <Stack direction="row" spacing={2} >
                            <Box className={classes.searchIcon}>
                                <SearchIcon />
                            </Box>
                            <Box className={classes.searchIcon}>
                                <NotificationsNoneOutlinedIcon />
                            </Box>
                        </Stack>
                    </Stack>
                    {/* portfolio component */}
                    <Portfolio />
                    {/* portfolio component */}
                    {/* favourites */}
                    <Stack direction="row" display={"flex"} justifyContent={"space-between"} alignItems={"center"} pt={6} pb={3}>
                        <Typography className={classes.favour}>Favorites</Typography>
                        <Typography className={classes.seeAll}>See All</Typography>
                    </Stack>
                    <Grid container spacing={2} >
                        {bitcoinData?.map((val) => {
                            return (
                                <Grid item xs={6} sm={6} md={6} lg={6}>
                                    <Bitcoin val={val} />
                                </Grid>
                            )
                        })
                        }
                    </Grid>
                    {/* favourites */}
                    {/* live prices */}
                    <Stack direction="row" alignItems={"center"} pt={4} pb={3}>
                        <Typography className={classes.favour}>Live Prices</Typography>
                    </Stack>
                    {
                        livePrices?.map((val) => {
                            return (
                                <LivePrices val={val} />
                            )
                        })
                    }
                    {/* live prices */}
                </Box>
            </Grid>
            {/* right side card */}
        </Grid >
    </div>
}