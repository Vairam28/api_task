import { makeStyles } from "@mui/styles";
const drawerWidth = 56;
export const useStyles = makeStyles((theme) => ({
    root: {
        width: props => props?.isMobile ? 240 : drawerWidth,
        position: 'absolute',
    },
    drawer: {
        height: props => props?.isMobile ? `100vh` : `calc(100vh - 64px)`,
        width: props => props?.isMobile ? 240 : drawerWidth,
        width: "72px",
        height: "100vh",
        backgroundColor: "white",
        position: "fixed",
        borderRight: "1px solid lightgrey",
        boxShadow: "none"
    },
    homeLogo: {
        color: "lightgrey",
    },
    addLogo: {
        backgroundColor: "#2C00D5",
        color: "white",
        borderRadius: "0px 8px 8px 8px",
        display: "flex",
        height: "32px",
        width: "32px",
        alignItems: "center",
        justifyContent: "center"
    }
}))