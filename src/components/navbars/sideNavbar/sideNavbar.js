import React from 'react';
import { useStyles } from "./style";
import { Paper, List, ListItem, ListItemIcon, ListItemText, Box, Stack } from '@mui/material';
import { matchPath, useNavigate, useLocation } from 'react-router-dom';
import HomeIcon from '@mui/icons-material/Home';
import PersonOutlineIcon from '@mui/icons-material/PersonOutline';
import SettingsOutlinedIcon from '@mui/icons-material/SettingsOutlined';
import StarOutlineOutlinedIcon from '@mui/icons-material/StarOutlineOutlined';
import RestoreIcon from '@mui/icons-material/Restore';
import ImportExportIcon from '@mui/icons-material/ImportExport';
import AddOutlinedIcon from '@mui/icons-material/AddOutlined';

export const SideNavBar = (props) => {

    const classes = useStyles(props);

    const navigate = useNavigate();
    const location = useLocation();

    const isSelected = (data) => {
        if (data.link) {
            return matchPath(location.pathname, {
                path: data.link
            })
        }
    }

    const onListClick = (data) => {
        if (data.link) {
            navigate(data.link)
        }
    }

    return (
        <div className={classes.root}>
            <Paper
                className={classes.drawer}
                square
            >
                <div className={classes.drawerContainer}>
                    <List>
                        {[].map((navBar, index) => (
                            <ListItem onClick={(e) => onListClick(navBar)}
                                button
                                key={index}
                                selected={isSelected(navBar)}>

                                <ListItemIcon>{navBar.icon}</ListItemIcon>

                                <ListItemText primary={navBar.name} />

                            </ListItem>
                        ))}
                    </List>
                </div>
                <Stack direction="column" spacing={8} alignItems="center">
                    <Stack direction="column" spacing={2} alignItems="center">
                        {/* <AllInclusiveIcon className={classes.logo} /> */}
                        <Box>
                            <img src="/images/topLogo.png" alt="logo" />
                        </Box>
                        <Box>
                            <HomeIcon className={classes.homeLogo} />
                        </Box>
                        <Box>
                            <PersonOutlineIcon className={classes.homeLogo} />
                        </Box>
                        <Box>
                            <img src="/images/message.png" alt="logo" />
                        </Box>
                        <Box>
                            <SettingsOutlinedIcon className={classes.homeLogo} />
                        </Box>
                        <Box>
                            <img src="/images/rightArrow.png" alt="logo" />
                        </Box>
                        <Box>
                            <StarOutlineOutlinedIcon className={classes.homeLogo} />
                        </Box>
                        <Box>
                            <RestoreIcon className={classes.homeLogo} />
                        </Box>
                        <Box>
                            <ImportExportIcon className={classes.homeLogo} />
                        </Box>
                    </Stack>
                    {/* settings */}
                    <Stack direction="column" alignItems="center" className={classes.settingMob}>
                        <Box className={classes.addLogo}>
                            <AddOutlinedIcon />
                        </Box>
                    </Stack>
                    {/* settings */}
                </Stack>
            </Paper>
        </div>
    );
}
