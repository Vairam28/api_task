import React from 'react';
import { Box, Typography, Stack } from '@mui/material';
import { useStyles } from "./style";


export const Bitcoin = ({
val= {}

}) => {

    const classes = useStyles();


    return <Box className={classes.root}>
        <Box className={classes.bitcoinCard}>
            <Stack direction={"row"} alignItems={"center"} spacing={3}>
                <Box className={classes.setLogo} sx={{backgroundColor:val?.coinBackground}} >
                    {val?.icon}
                </Box>
                <Box>
                    <Typography className={classes.title}>
                        {val?.title}
                    </Typography>
                    <Typography className={classes.subTitle}>
                        {val?.subTitle}
                    </Typography>
                </Box>
            </Stack>
            <img src={val?.img} alt="graph" />
            <Box display={"flex"} alignItems={"center"} justifyContent={"space-between"} >
                <Typography className={classes.amountInDollar}>{val?.dollar}</Typography>
                <Typography className={classes.percentage}>{val?.percentage}</Typography>
            </Box>
        </Box>
    </Box>

}