import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    setLogo: {
        color: "white",
        borderRadius: "50%",
        display: "flex",
        height: "45px",
        width: "45px",
        alignItems: "center",
        justifyContent: "center",

        "& .css-i4bv87-MuiSvgIcon-root": {
            width: "21px",
        }
    },
    title:{
        color:"#2D2D2D",
        fontWeight:"bold",
        fontSize:"18px"
    },
    subTitle:{
        color:"#C1C2C6",
        fontSize:"12px"
    },
    bitcoinCard:{
        backgroundColor: "#FFFFFF",
        borderRadius:"8px",
        padding:"18px"
    },
    amountInDollar:{
        fontSize:"17px",
        color:"black",
        fontWeight:"bold"
    },
    percentage:{
        fontSize:"14px",
        color:"#80B99E"
    }

}))