import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    portfolioCard: {
        background: "linear-gradient(to right, #2B00D5, #2424DA)",
        borderRadius: "8px",
        padding: "18px",
        height:"120px"
    },
    profileName: {
        color: "#FFFFFf",
        fontSize: "14px"
    },
    dollarName: {
        color: "#FFFFFf",
        fontSize: "20px",
        fontFamily:"crayond_bold"
        
    },
    signalCellularIcon: {
        border: "1px solid white",
        width: "26px",
        height: "26px",
        borderRadius: "8px",
        margin: "0 0 0 auto",
        "& .css-i4bv87-MuiSvgIcon-root": {
            width: "15px",
            color: "#FFFFFF",
            marginLeft: "4px",
        }
    },
    percentage:{
        color:"#FFFFFF",
        fontSize:"14px"
    },
    rotate:{
        rotate:"180deg"
    },
    buttonDeposit:{
        color:"black",
        backgroundColor:"white",
        borderRadius:"10px",
        textTransform:"Initial"
    },
    root:{
        position:"relative"
    },
    buttonParent:{
        position: "absolute",
        bottom: "-17px",
        right: "48px",
        [theme.breakpoints.only("sm")]: {
            right: "235px",
         }
    },

}))