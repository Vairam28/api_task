import React from 'react';
import { Box, Typography, Stack, IconButton, Button } from '@mui/material';
import { useStyles } from "./style";
import SignalCellularAltIcon from '@mui/icons-material/SignalCellularAlt';
import FileDownloadOutlinedIcon from '@mui/icons-material/FileDownloadOutlined';

export const Portfolio = ({


}) => {

    const classes = useStyles();


    return <Box className={classes.root}>
        <Box className={classes.portfolioCard} >
            <Box display={"flex"} alignItems={"center"} justifyContent={"space-between"}>
                <Box>
                    <Typography className={classes.profileName}>
                        My Portfolio
                    </Typography>
                    <Typography className={classes.dollarName} pt={1.5}>
                        $34,140.10
                    </Typography>
                </Box>
                <Box>
                    <Box className={classes.signalCellularIcon}>
                        <SignalCellularAltIcon />
                    </Box>
                    <Typography className={classes.percentage} pt={1.5}>
                        +2.5%
                    </Typography>
                </Box>
            </Box>
        </Box>
        <Stack direction={"row"} alignItems={"center"} justifyContent={"center"} spacing={2} className={classes.buttonParent} >
            <Button variant="contained" className={classes.buttonDeposit}> <FileDownloadOutlinedIcon mr={2} /> Deposit</Button>
            <Button variant="contained" className={classes.buttonDeposit}> <FileDownloadOutlinedIcon className={classes.rotate} /> Withdraw</Button>
        </Stack>
    </Box>

}