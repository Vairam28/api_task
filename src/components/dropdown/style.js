import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    dropdown: {
        fontSize:"14px",
        backgroundColor:"#F3F5F9",
        border:"none",
        borderRadius: "10px",
        "& .css-11u53oe-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.css-11u53oe-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.css-11u53oe-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input": {
            padding: "12px 14px",
            fontSize: "14px",
            borderRadius: "10px",
            textAlign: "initial",
            paddingRight:"0px",
            border:"none",
            
        },
        "& .css-11u53oe-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input":{
            fontWeight:"bold",
            color:"#5A5A5A"
        },
        "& .css-jedpe8-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.css-jedpe8-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input.css-jedpe8-MuiSelect-select-MuiInputBase-input-MuiOutlinedInput-input":{
            paddingRight:"0px"
        }
    },
    dropdownIcon:{
        width:"100%",
        "& .css-hfutr2-MuiSvgIcon-root-MuiSelect-icon":{
            color: "darkgrey",
        },
        "& .css-1d3z3hw-MuiOutlinedInput-notchedOutline":{
            borderRadius: "10px",
            border:"none"
        },
        "& .css-yf8vq0-MuiSelect-nativeInput":{
            position:"relative"
        }
    }

}))