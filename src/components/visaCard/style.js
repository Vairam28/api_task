import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    visa:{
        padding:"20px",
        borderRadius:"8px",
        background: "linear-gradient(221deg, #F7D16C,#F4BA60)",
    },
    cardName:{
        color:"#1B1108",
        fontSize:"11px",
        paddingBottom:"2px"
    },
    profileName:{
        color:"#1B1108",
        fontSize:"16px",
        fontFamily:"crayond_bold"
    },
    logo:{
        color:"#1B1108",
        fontSize:"26px",
        fontWeight:"bold",
    },
    password:{
        color:"#1B1108",
        fontSize:"14px",
        fontFamily:"crayond_bold"
    },
    date:{
        color:"lightslategrey",
        fontSize:"11px",
        fontFamily:"crayond_bold",
        paddingBottom:"4px"
    },
    number:{
        color:"#1B1108",
        fontSize:"14px",
        fontWeight:"bold"
    },
    root:{
        paddingTop:"50px",
        [theme.breakpoints.only("xs")]: {
            paddingTop:"30px",
         }
    }
}))