import React from 'react';
import { Box, Typography, Stack } from '@mui/material';
import { useStyles } from "./style";

export const VisaCard = () => {

    const classes = useStyles();


    return <Box className={classes.root}>
        <Box direction={"row"} alignItems={"center"} className={classes.visa} >
            <Stack direction={"row"} alignItems={"center"} justifyContent={"space-between"} pb={2} >
                <Box>
                    <Typography className={classes.cardName}>
                        Name Card
                    </Typography>
                    <Typography className={classes.profileName}>
                        Vairam
                    </Typography>
                </Box>
                <Box className={classes.logo}>
                   <img src="/images/visaLogo.png" alt="visaLogo" />
                </Box>
            </Stack>
            <Box pb={2}>
                <Typography className={classes.password}>
                    1234 5678 1234 1235
                </Typography>
            </Box>
            <Stack direction={"row"} alignItems={"center"} spacing={3}>
                <Box>
                    <Typography className={classes.date}>
                        EXP DATE
                    </Typography>
                    <Typography className={classes.number}>
                        12/25
                    </Typography>
                </Box>
                <Box>
                    <Typography className={classes.date}>
                        CCV NUMBER
                    </Typography>
                    <Typography className={classes.number}>
                        123
                    </Typography>
                </Box>
            </Stack>
        </Box>
    </Box>

}