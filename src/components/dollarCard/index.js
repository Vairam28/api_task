import React from 'react';
import { Box, Typography, Stack } from '@mui/material';
import { useStyles } from "./style";


export const DollarCard = ({
val={}

}) => {

    const classes = useStyles();


    return <Box className={classes.root}>
        <Stack direction={"row"} alignItems={"center"} spacing={2} >
            <Box className={classes.dollarLogo} sx={{backgroundColor:val?.backgroundColor}} >
                {val?.icon}
            </Box>
            <Box>
                <Typography className={classes.total}> 
                    {val?.title}
                </Typography>
                <Typography className={classes.dollar}>
                    {val?.subTitle}
                </Typography>
            </Box>
        </Stack>
    </Box>

}