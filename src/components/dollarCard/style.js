import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    dollarLogo: {
        color: "#335A4B",
        borderRadius: "8px",
        display: "flex",
        height: "55px",
        width: "55px",
        alignItems: "center",
        justifyContent: "center",
        
        "& .css-i4bv87-MuiSvgIcon-root":{
            width: "21px",
        }
    },
    total:{
        fontSize:"11px",
        color:"#A0A3A7",
        paddingBottom:"4px"
    },
    dollar:{
        color:"#050505",
        fontWeight:"bold",
        fontSize:"19px"
    },
    root:{
        paddingTop:"58px",
        [theme.breakpoints.only("xs")]: {
            paddingTop:"30px",
         }
    }
}))