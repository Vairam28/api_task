import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    box: {
        width: "20px",
        height: "20px",
        borderRadius: "6px"
    },
    dot: {
        width: "5px",
        height: "5px",
        borderRadius: "50%",
        margin: "auto",
        marginTop: "7px"
    },
    verticalIcon: {
        backgroundColor: "#F7F5F9",
        borderRadius: "6px",
        display: "flex",
        padding: "2px 6px",
        color: "darkgrey",
        "& .css-i4bv87-MuiSvgIcon-root":{
            width: "15px",
        }
    },
    title:{
        color:"#2D2D2D",
        fontWeight:"bold",
        fontSize:"15px",
        paddingBottom:"4px"
    },
    subTitle:{
        color:"#C1C2C6",
        fontSize:"10px"
    },
    jobTitle:{
        color:"#717171",
        fontSize:"12px"
    },
    dollar:{
        fontWeight:"bold",
        fontSize:"15px"
    },
    decimalNumbers:{
        fontWeight:"bold",
        fontSize:"11px"
    },
    moreVertIcon:{
        "& .css-i4bv87-MuiSvgIcon-root":{
            color: "#C2C4C8"
        }
    }
}))