import React from 'react';
import { Box, Typography, Stack, IconButton } from '@mui/material';
import { useStyles } from "./style";
import AttachFileOutlinedIcon from '@mui/icons-material/AttachFileOutlined';

export const HistoryCard = ({
    val = {}

}) => {

    const classes = useStyles();


    return <Box className={classes.root}>
        <Box display={"flex"} alignItems={"center"} justifyContent={"space-between"} className={classes.history} pb={2.8}>
            <Stack direction={"row"} alignItems={"center"} spacing={2}>
                <Box sx={{color:val?.iconColor}}>
                    {val?.icon}
                </Box>
                <Box>
                    <Typography className={classes.title}>
                        {val?.title}
                    </Typography>
                    <Typography className={classes.subTitle}>
                        {val?.subTitle}
                    </Typography>
                </Box>
            </Stack>
            <Stack direction={"row"} alignItems={"center"} spacing={1}>
                <Box className={classes.box} sx={{backgroundColor:val?.dotBackground}} >
                    <Box className={classes.dot} sx={{backgroundColor:val?.dot}} >

                    </Box>
                </Box>
                <Box className={classes.jobTitle}>
                    {val?.jobTitle}
                </Box>
            </Stack>
            <Stack direction={"row"} alignItems={"center"} spacing={2.5}>
                <Box className={classes.verticalIcon}>
                    <AttachFileOutlinedIcon className={classes.moreVertIcon} />
                </Box>
                <Box>
                    <Typography className={classes.dollar} sx={{color:val?.amountColor}}>
                        {val?.dollarNumber}<span className={classes.decimalNumbers} sx={{color:val?.amountColor}}>.00</span>
                    </Typography>
                </Box>
            </Stack>
        </Box>
    </Box>

}