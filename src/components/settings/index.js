import React from 'react';
import { Box, Typography, Stack } from '@mui/material';
import { useStyles } from "./style";

export const SettingsCard = ({
val={}

}) => {

    const classes = useStyles();


    return <Box className={classes.root}>
        <Box direction={"row"} alignItems={"center"} className={classes.visa} >
            <Stack direction={"row"} alignItems={"center"} justifyContent={"space-between"} pb={3} >
                <Box display={"flex"} alignItems={"center"} >
                    <Box className={classes.setLogo}>
                        {val?.icon}
                    </Box>
                    <Typography className={classes.settingName} pl={3}>
                        {val?.title}
                    </Typography>
                </Box>
                <Box className={classes.logo}>
                    {val?.arrow}
                </Box>
            </Stack>
        </Box>
    </Box>

}