import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    setLogo: {
        backgroundColor: "#EEEAFF",
        color: "blueviolet",
        borderRadius: "8px",
        display: "flex",
        height: "45px",
        width: "45px",
        alignItems: "center",
        justifyContent: "center",

        "& .css-i4bv87-MuiSvgIcon-root": {
            width: "21px",
        }
    },
    settingName: {
        color: "#9D9D9D"
    }
}))