import { makeStyles } from "@mui/styles";

export const useStyles = makeStyles((theme) => ({
    title:{
        color:"#2D2D2D",
        fontSize:"14px",
        fontWeight:"bold"
    },
    subTitle:{
        color:"#C1C2C6",
        fontSize:"12px"
    },
    jobTitle:{
        color:"#717171",
        fontSize:"12px"
    },
    dollar:{
        color:"#2D2D2D",
        fontWeight:"bold",
        fontSize:"16px"
    },
    decimalNumbers:{
        color:"#2D2D2D",
        fontWeight:"bold",
        fontSize:"11px"
    },
    moreVertIcon:{
        "& .css-i4bv87-MuiSvgIcon-root":{
            color: "#C2C4C8"
        }
    },
    setLogo: {
        color: "white",
        borderRadius: "50%",
        display: "flex",
        height: "45px",
        width: "45px",
        alignItems: "center",
        justifyContent: "center",

        "& .css-i4bv87-MuiSvgIcon-root": {
            width: "22px",
        }
    },
    percentage:{
        fontSize:"14px",
    },
    root:{
        paddingBottom:"26px"
    },
    graphChart:{
        width:"79px"
    }
}))