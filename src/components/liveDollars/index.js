import React from 'react';
import { Box, Typography, Stack, IconButton } from '@mui/material';
import { useStyles } from "./style";


export const LivePrices = ({
    val = {}

}) => {

    const classes = useStyles();


    return <Box className={classes.root}>
        <Box display={"flex"} alignItems={"center"} justifyContent={"space-between"} className={classes.history} >
            <Stack direction={"row"} alignItems={"center"} spacing={2}>
                <Box className={classes.setLogo} sx={{ backgroundColor: val?.backgroundColor }} >
                    {val?.icon}
                </Box>
                <Box>
                    <Typography className={classes.title}>
                        {val?.title}
                    </Typography>
                    <Typography className={classes.subTitle}>
                        {val?.subTitle}
                    </Typography>
                </Box>
            </Stack>
            <Stack direction={"row"} alignItems={"center"} spacing={3}>
                <Box className={classes.verticalIcon}>
                    <img className={classes.graphChart} src={val?.img} alt="chart" />
                </Box>
                <Box>
                    <Typography className={classes.dollar}>
                        {val?.amount}
                    </Typography>
                    <Typography className={classes.percentage} sx={{color:val?.fontColor}} >
                        {val?.percentage}
                    </Typography>
                </Box>
            </Stack>
        </Box>
    </Box>

}